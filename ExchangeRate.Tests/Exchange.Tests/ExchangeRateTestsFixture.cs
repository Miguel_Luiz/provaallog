﻿using Bogus;
using Prova;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.Tests.Exchange.Tests
{
    public class ExchangeRateTestsFixture
    {
        public ExchangeRate GenerateInvalidFactorExchangeRate()
        {
            return new Faker<ExchangeRate>("pt_BR").CustomInstantiator(f => new ExchangeRate(
                new Faker().Date.RecentDateOnly(20),
                f.Random.Decimal(-2, 0),
                f.Random.Guid(),
                f.Random.Guid(),
                f.Random.Guid()
            ));
        }

        public ExchangeRate GenerateInvalidTypeIdExchangeRate()
        {
            return new ExchangeRate(
                new DateOnly(2020, 2, 8),
                2.3333m,
                new Guid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            );
        }

        public ExchangeRate GenerateFactorWithMoreThan5DecimalsExchangeRate()
        {
            return new ExchangeRate(
                new DateOnly(2020, 2, 8),
                2.333333333333333m,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            );
        }

        public ExchangeRate GenerateQuotationDateInTheFutureExchangeRate()
        {
            return new ExchangeRate(
                new DateOnly(2026, 2, 8),
                2.333333333333333m,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            );
        }
    }
}
