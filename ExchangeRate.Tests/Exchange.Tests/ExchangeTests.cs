using ExchangeRates.Tests.Exchange.Tests;
using Prova;
using System.Globalization;

namespace ExchangeRates.Tests;

public class ExchangeTests : IClassFixture<ExchangeRateTestsFixture>
{
    private readonly ExchangeRateTestsFixture _fixture;

    public ExchangeTests(ExchangeRateTestsFixture fixture)
    {
        _fixture = fixture;
    }
    [Fact(DisplayName = "Teste de Factor sendo menor que zero")]
    public void Validate_FactorLesThanZero_ShouldReturnException()
    {
        var exception = Record.Exception(() => _fixture.GenerateInvalidFactorExchangeRate());
        
        Assert.IsType<DomainException>(exception);
        Assert.Equivalent("The Factor cannot be zero", exception.Message); 
    }

    [Fact(DisplayName = "Teste de TypeId sendo nulo")]
    public void Validate_TypeIdIsNull_ShouldReturnException()
    {
        //var exchangeRate = _fixture.GenerateInvalidFactorExchangeRate();

        var exception = Record.Exception(() => _fixture.GenerateInvalidTypeIdExchangeRate());

        Assert.IsType<DomainException>(exception);
        Assert.Equivalent("The TypeId cannot be empty", exception.Message);
    }

    

    [Fact(DisplayName = "Teste de casas decimais após avírgula de Factor")]
    public void Validate_FactorDecimalsAreMoreThan5_ShouldHave5Decimals()
    {
        var exchangeRate = _fixture.GenerateFactorWithMoreThan5DecimalsExchangeRate();

        int count = BitConverter.GetBytes(decimal.GetBits(exchangeRate.Factor)[3])[2];

        // Console.WriteLine($"decimais {count}++++++++++++++++");
        Assert.Equal(5, count);

    }

    [Fact(DisplayName = "Teste de QuotationDate sendo no futuro")]
    public void Validate_QuotationDateIsInTheFuture_ShouldReturnException()
    {
        var exception = Record.Exception(() => _fixture.GenerateQuotationDateInTheFutureExchangeRate());

        Assert.IsType<DomainException>(exception);
        Assert.Equivalent("The QuotationDate cannot be in the future", exception.Message);
    }
}